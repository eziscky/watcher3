#!/usr/bin/python3

import toml
import sys

class Parser:
    def __init__(self,conf):
        self.conf = conf
        self.doc = None
        self._load_file()

    def _load_file(self):
        with open(self.conf) as stream:
            try:
                self.doc = toml.load(stream)
            except tom:
                print("Error parsing toml",sys.exec_info()[0])
                return

    def get_values(self):
        return self.doc

