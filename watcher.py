#!/usr/bin/python3

import os
import sys
import time
import fcntl
import subprocess
from parser.parse import *
from parser.fields import *
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class Handler(FileSystemEventHandler):
    def __init__(self, base_path, fnames):
        self.fnames = fnames
        self.base_path = base_path

    def process(self, event):
        for fname in self.fnames.keys():
            if event.src_path == os.path.join(self.base_path, fname):
                self.deploy(fname)

    def deploy(self, binary):
        ret = subprocess.call(self.fnames[binary][CMD], shell=True)
        if ret != 0:
            print("{:s} Failed".format(self.fnames[binary][CMD]))
        print("{:s} Succesful".format(self.fnames[binary][CMD]))

    def on_modified(self, event):
        if not event.is_directory:
            if event.event_type == 'modified' or event.event_type == 'created':
                self.process(event)
        
def run():
    obs.schedule(handler, path)
    obs.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        obs.stop()
    
    obs.join()



def daemonize(func,wdir,stdout="/tmp/watcher.out",stderr="/tmp/watcher.err"):
    process_id = os.fork()
    if process_id < 0:
        # Fork error.  Exit badly.
        sys.exit(1)
    elif process_id != 0:
        # This is the parent process.  Exit.
        sys.exit(0)
    # This is the child process.  Continue.

    process_id = os.setsid()
    if process_id == -1:
        sys.exit(1)
    
    sout = open(stdout,"w+")
    serr = open(stderr,"w+")
    os.dup2(sout.fileno(),sys.stdout.fileno())
    os.dup2(serr.fileno(),sys.stderr.fileno())   
    
    os.chdir(wdir)
    # Set umask to default to safe file permissions when running
    os.umask(0o027)

    # Create a pid file,uncomment below line to allow only one instance
    lockfile = open('/tmp/watcher.lock', 'w')
    # fcntl.lockf(lockfile, fcntl.LOCK_EX|fcntl.LOCK_NB)
    lockfile.write('%s' %(os.getpid()))
    lockfile.flush()
    func()
    
    
def check_path(path):
    try:
        os.stat(path)
    except:
        print("can't find {:s}".format(path))
        return False
    if not os.path.isdir(path):
        print("{:s} is not a directory".format(path))
        return False
    return True

def print_usage():
    print("./watcher.py /path/to/conf.toml [-d]")
    
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print_usage()
        sys.exit(1)
        
    watchconf = sys.argv[1]
    daemon = False
    if len(sys.argv) > 2:
        if sys.argv[2] == "-d":
            daemon = True
    
    try:
        os.stat(watchconf)
    except:
        print("{:s} does not exist".format(watchconf))
        sys.exit(1)
    
    parser = Parser(watchconf)
    content = parser.get_values()

    if not check_path(content[LOGPATH]):
        sys.exit(1)
    
    if not check_path(content[PATH]):
        sys.exit(1)
    
    path = content[PATH]
    obs = Observer()
    handler = Handler(fnames=content[WATCHLIST], base_path=path)
    if daemon:
        daemonize(run,path,stdout=os.path.join(content[LOGPATH],content[STDOUT]),stderr=os.path.join(content[LOGPATH],content[STDERR]))
    else:
        run()
